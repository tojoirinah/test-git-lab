using System;
using System.Reflection;

namespace TestCI_CD_Jenkins_Gitlab.Application.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}