﻿using System.Web;
using System.Web.Mvc;

namespace TestCI_CD_Jenkins_Gitlab.Application
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
